
package com.anson.linkvictoryword;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ClipData.Item;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;





import com.anson.linkvictoryword.R;
import com.anson.linkvictoryword.news.NewsActivity;

import java.util.ArrayList;


public class MainActivity extends Activity implements OnClickListener{
    private Button mLoginBtn;
    private Button mPageBtn;
    private TextView mShowTv;
    public static final String URL = "http://shoe.jamzoo.com.tw/App/Statistics?Type=0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mLoginBtn = (Button)findViewById(R.id.login_btn);
        mPageBtn = (Button)findViewById(R.id.page_btn);
        mShowTv = (TextView)findViewById(R.id.show_tv);
        mLoginBtn.setOnClickListener(this);
        mPageBtn.setOnClickListener(this);
    }
    
    
    

    /**取得Google帳號*/
    private ArrayList<Item> getAccount() {
        ArrayList<Item> accountsList = new ArrayList<Item>();
        try {
            Account[] accounts = AccountManager.get(this).getAccountsByType("com.google");
            for (Account account : accounts) {
                Item item = new Item(account.name);
                accountsList.add(item);
            }
        } catch (Exception e) {
            Log.i("Exception", "Exception:" + e);
        }
        return accountsList;
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_btn:
                ArrayList<Item> array = getAccount();
                for(Item item : array){
                    System.out.println(item.getText().toString());
                }
                MainAsyncTask task = new MainAsyncTask(mShowTv,MainActivity.this);
                task.execute(URL);
                break;
            case R.id.page_btn:
                Intent intent = new Intent(MainActivity.this,NewsActivity.class);
                MainActivity.this.startActivity(intent);
                break;
        }
    }
    
    
    
}
