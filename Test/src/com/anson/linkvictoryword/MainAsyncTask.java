package com.anson.linkvictoryword;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;

import com.anson.linkvictoryword.data.MyOpenHelper;
import com.anson.linkvictoryword.data.MySharedPreferences;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class MainAsyncTask extends AsyncTask<String, Void, String>{
    public TextView mTextView ;
    public Context mContext ;
    private MyOpenHelper mOpenHelper;
    private MySharedPreferences mSharedPreferences;
    public MainAsyncTask (TextView textView,Context context ){
        mTextView = textView;
        mContext = context;
        mOpenHelper = new MyOpenHelper(mContext);
        mSharedPreferences = new MySharedPreferences(context);
    }
    
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
    
    
    @Override
    protected String doInBackground(String... params) {
        String a = getJSONFromUrl(params[0]);
        return a;
    }

    @Override
    protected void onPostExecute(String a) {
        mSharedPreferences.setJSON(a);
        try {
            JSONObject jObj = new JSONObject(a);
            JSONArray array = jObj.getJSONArray("Rank");
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i) ;
                String id = obj.getString("Id");
                String times = obj.getString("Times");
                
                System.out.println("Id="+id);
                System.out.println("Times="+times);
                System.out.println("-------------------------=");
                
                mOpenHelper.add(id, times);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        
        System.out.println(a);
        mTextView.setText(a);
        super.onPostExecute(a);
    }




    /** 取得JSON資料-(使用GET) */
  public static String getJSONFromUrl(String url) {
      url = url.replaceAll(" ", "");//去除空白
      String result = "";
      HttpClient client = new DefaultHttpClient();
      HttpGet get = new HttpGet(url);
      try {
          HttpResponse response = client.execute(get);
          HttpEntity resEntity = response.getEntity();
          result = EntityUtils.toString(resEntity);
      } catch (ClientProtocolException e) {
          //MyLog.printSystem("error : ClientProtocolException");
      } catch (IOException e) {
         // MyLog.printSystem("error : IOException");
      } finally {
          client.getConnectionManager().shutdown();
      }
      return result;
  }
}
