package com.anson.linkvictoryword.news;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;


public class NewsData {
    private String id;
    private String title;
    private String content;
    private String link;
    private Bitmap photo;
    
    
    public NewsData (JSONObject jObj){
        try {
            id = jObj.getString("Id");
            title = jObj.getString("Title");
            content = jObj.getString("Content");
            byte[] image = Base64.decode(jObj.getString("Photo").getBytes(),0);
            photo = BitmapFactory.decodeByteArray(image,0,image.length);
            link = jObj.getString("Link");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }

    /**ID值*/
    public String getId() {
        return id;
    }

    /**標題*/
    public String getTitle() {
        return title;
    }

    /**內文*/
    public String getContent() {
        return content;
    }

    /**圖片*/
    public Bitmap getPhoto() {
        return photo;
    }
    
    
    /**連結*/
    public String getLink() {
        return link;
    }
}
