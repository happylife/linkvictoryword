
package com.anson.linkvictoryword.news;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.anson.linkvictoryword.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NewsActivity extends Activity {
    private ListView mListView;
    private ProgressBar mProgressBar;
    private String LINE_PACKAGE_NAME = "jp.naver.line.android";
    private String FB_PACKAGE_NAME = "com.facebook.katana";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_news);
        mListView = (ListView) findViewById(R.id.listview);
        mProgressBar = (ProgressBar) findViewById(R.id.progress);
        NewsTask task = new NewsTask();
        task.execute();

        super.onCreate(savedInstanceState);
    }

    private class NewsTask extends AsyncTask<Void, Void, ArrayList<NewsData>> {

        @Override
        protected void onPreExecute() {
            mProgressBar.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }

        @Override
        protected ArrayList<NewsData> doInBackground(Void... params) {
            ArrayList<NewsData> newsArray = new ArrayList<NewsData>();

            String result = getJSONFromUrl("http://demo.ourpower.com.tw:8082/api/news");

            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("News");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jObj = jsonArray.getJSONObject(i);
                    newsArray.add(new NewsData(jObj));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return newsArray;
        }

        @Override
        protected void onPostExecute(ArrayList<NewsData> result) {
            NewsAdapter adapter = new NewsAdapter(NewsActivity.this, 0, 0, result);
            mListView.setAdapter(adapter);
            mListView.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    NewsData data = (NewsData) parent.getItemAtPosition(position);

                    if (checkAppInstalled(LINE_PACKAGE_NAME)) {
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setPackage(LINE_PACKAGE_NAME);
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_TEXT, data.getLink());
                        startActivity(intent);
                    }
                }
            });
            mProgressBar.setVisibility(View.INVISIBLE);
            super.onPostExecute(result);
        }

        /** 取得JSON資料-(使用GET) */
        public String getJSONFromUrl(String url) {
            url = url.replaceAll(" ", "");// 去除空白
            String result = "";
            HttpClient client = new DefaultHttpClient();
            HttpGet get = new HttpGet(url);
            get.setHeader("Authorization", "1945a2f51a0");
            try {
                HttpResponse response = client.execute(get);
                HttpEntity resEntity = response.getEntity();
                result = EntityUtils.toString(resEntity);
            } catch (ClientProtocolException e) {
                // MyLog.printSystem("error : ClientProtocolException");
            } catch (IOException e) {
                // MyLog.printSystem("error : IOException");
            } finally {
                client.getConnectionManager().shutdown();
            }
            return result;
        }
    }

    /** ListView Adapter */
    private class NewsAdapter extends ArrayAdapter<NewsData> {
        private ArrayList<NewsData> mList;
        private LayoutInflater mInflater;

        public NewsAdapter(Context context, int resource, int textViewResourceId,
                ArrayList<NewsData> list) {
            super(context, resource, textViewResourceId, list);
            mList = list;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View layout = mInflater.inflate(R.layout.item_news, null);
            NewsData data = mList.get(position);
            ImageView newsIv = (ImageView) layout.findViewById(R.id.news_iv);
            TextView titleTv = (TextView) layout.findViewById(R.id.title_tv);
            TextView contentTv = (TextView) layout.findViewById(R.id.content_tv);

            // 好像是圖片Cache
            // newsIv.setDrawingCacheEnabled(true);
            // newsIv.buildDrawingCache();

            newsIv.setImageBitmap(data.getPhoto());
            titleTv.setText(data.getTitle());
            contentTv.setText(data.getContent());

            return layout;
        }

    }

    
    /** 檢查APP是否有安裝 */
    private boolean checkAppInstalled(String packageName) {
        PackageManager pm = getPackageManager();
        List<ApplicationInfo> list = pm.getInstalledApplications(0);
        boolean lineInstallFlag = false;
        for (ApplicationInfo ai : list) {
            if (ai.packageName.equals(packageName)) {
                lineInstallFlag = true;
                break;
            }
        }
        return lineInstallFlag;
    }
}
