package com.anson.linkvictoryword.data;

import android.content.Context;
import android.content.SharedPreferences;

/**SharedPreferencest 資料*/
public class MySharedPreferences {
    private SharedPreferences mSp;
    public MySharedPreferences(Context context){
        mSp = context.getSharedPreferences("volvo", Context.MODE_PRIVATE);
    }
    /**紀錄JSON文字*/
    public void setJSON(String json){
        mSp.edit().putString("JSON", json).commit();
    }
    /**取得JSON文字*/
    public String getJSON(){
        return mSp.getString("JSON",null);
    }
}
