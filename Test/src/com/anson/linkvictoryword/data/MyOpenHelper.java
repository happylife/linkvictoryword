package com.anson.linkvictoryword.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**資料庫*/
public class MyOpenHelper extends SQLiteOpenHelper{

    /** 資料庫名稱 */
    public static final String DATABASE_NAME = "EasyNote2";
    
    /** 資料庫版本 */
    public static final int DATABASE_VERSION = 1;

    /** 表格名 */
    public static final String TABLE_NAME = "text";

    /** 流水號ID */
    public static final String AUTO_ID = "auto_id";

    /**ID*/
    public static final String ID = "id";

    /**時間*/
    public static final String TIMES = "times";



    public static final String SQL_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_NAME
            + " (" + AUTO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT"
            + ", " + ID + " INTEGER"
            + ", " + TIMES + " TEXT"+")";
    
    
    public MyOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        
    }
    
    /**增*/
    public void add(String id,String times){
        ContentValues values = new ContentValues();
        values.put(ID, id);
        values.put(TIMES, times);
        this.getWritableDatabase().insert(TABLE_NAME, null, values);
    }

}
